# 陆羽跨连协议玺链通讯插件

# 获取插件

+ 本插件使用maven构建，获取插件后，在插件目录下运行
    ```shell
    mvn install
    ```
+ 在target目录下 获取jar `sealchain-plugin-1.0.jar`

# 插件部署

+ 部署以及使用请联系工商银行软件开发中心区块链团队