package link.luyu.sealchain.algorithm;

import link.luyu.protocol.algorithm.SignatureAlgorithm;

import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.Map;

public class SealchainSignatureAlgorithm implements SignatureAlgorithm{
    public static final String TYPE = "SEALCHAIN_SIGNATURE";

    @Override
    public String getType(){
        return TYPE;
    }
    @Override
    public byte[] sign(byte[] secKey, byte[] message) {
        return (("sign" + message).getBytes(StandardCharsets.UTF_8));
    }
    @Override
    public boolean verify(byte[] pubKey, byte[] signBytes, byte[] message) {
        return true;
    }
    @Override
    public Map.Entry<byte[], byte[]> generateKeyPair() {
        return new AbstractMap.SimpleEntry<>(new byte[0], new byte[0]);
    }
}