package link.luyu.sealchain.conf;

import java.util.Map;

public class Properties {
    private String chainPath;
    private String ip;
    private String port;
    private String networkId;
    private String chainName;
    private String channelId;
    private String chaincodeName;
    private String user;
    private String org;
    private Boolean eventEnabled;
    private String eventSubUsername;
    private String eventHost;
    private String eventType;
    private String eventPort;
    private String eventExposePort;

    public Properties() {
    }

    public Properties(Map<String, Object> properties) {
        this.chainPath = (String) properties.get("chainPath");
        Map<String, Object> sealchainServices = (Map<String, Object>) properties.get("sealchainServices");
        this.ip = (String) sealchainServices.get("ip");
        this.port = (String) sealchainServices.get("port");
        this.networkId = (String) sealchainServices.get("networkId");
        this.chainName = (String) sealchainServices.get("chainName");
        this.channelId = (String) sealchainServices.get("channelId");
        this.chaincodeName = (String) sealchainServices.get("chaincodeName");
        this.user = (String) sealchainServices.get("user");
        this.org = (String) sealchainServices.get("org");
        this.eventEnabled = (Boolean) sealchainServices.get("eventEnabled");
        this.eventType = (String) sealchainServices.get("eventType");
        this.eventSubUsername = (String) sealchainServices.get("eventSubUsername");
        this.eventHost = (String) sealchainServices.get("eventHost");
        this.eventPort = (String) sealchainServices.get("eventPort");
        this.eventExposePort = (String) sealchainServices.get("eventExposePort");
    }

    public String getIp() {
        return ip;
    }

    public String getPort() {
        return port;
    }

    public String getNetworkId() {
        return networkId;
    }

    public String getChainName() {
        return chainName;
    }

    public String getChannelId() {
        return channelId;
    }

    public String getChaincodeName() {
        return chaincodeName;
    }

    public String getUser() {
        return user;
    }

    public String getOrg() {
        return org;
    }

    public String getChainPath() {
        return chainPath;
    }

    public Boolean getEventEnabled() {
        return eventEnabled;
    }

    public String getEventSubUsername() {
        return eventSubUsername;
    }

    public String getEventHost() {
        return eventHost;
    }

    public String getEventType() {
        return eventType;
    }

    public String getEventPort() {
        return eventPort;
    }

    public String getEventExposePort() {
        return eventExposePort;
    }

    @Override
    public String toString() {
        return "Properties{" +
                "chainPath='" + chainPath + '\'' +
                ", ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", networkId='" + networkId + '\'' +
                ", chainName='" + chainName + '\'' +
                ", channelId='" + channelId + '\'' +
                ", chaincodeName='" + chaincodeName + '\'' +
                ", user='" + user + '\'' +
                ", org='" + org + '\'' +
                ", eventEnabled=" + eventEnabled +
                ", eventSubUsername='" + eventSubUsername + '\'' +
                ", eventHost='" + eventHost + '\'' +
                ", eventType='" + eventType + '\'' +
                ", eventPort='" + eventPort + '\'' +
                ", eventExposePort='" + eventExposePort + '\'' +
                '}';
    }
}

