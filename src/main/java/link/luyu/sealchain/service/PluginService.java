package link.luyu.sealchain.service;

import com.moandjiezana.toml.Toml;
import link.luyu.protocol.link.Driver;
import link.luyu.sealchain.api.ChainProfile;
import link.luyu.sealchain.api.ChainState;
import link.luyu.sealchain.api.Dashboard;
import link.luyu.sealchain.conf.Properties;
import link.luyu.sealchain.link.SealchainDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PluginService {
    private static final Logger logger = LoggerFactory.getLogger(PluginService.class);
    private List<ChainProfile> chainProfileList;

    public Dashboard drawDashboard(){
        Dashboard dashboard = new Dashboard();
        dashboard.setChainProfiles(listChainProfiles());
        dashboard.setChainState(getChainState());
        dashboard.setLocalChainInfo(PluginContext.getInstance(Properties.class));
        return dashboard;
    }

    private ChainState getChainState() {
        ChainState chainState = new ChainState();
        Driver driver = PluginContext.getInstance(SealchainDriver.class);
        chainState.setBlockNumber(driver.getBlockNumber());
        return chainState;
    }

    private List<ChainProfile> listChainProfiles() {
        if (this.chainProfileList == null){
            this.chainProfileList = new ArrayList<>();
            URL resource = this.getClass().getClassLoader().getResource("chains");
            String chainsRootPath = resource.getPath();
            logger.info("[Plugin] chainRootPath:{}", chainsRootPath);
            File chainRootDirectory = new File(chainsRootPath);
            File[] plugins = chainRootDirectory.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.equals("plugin.toml");
                }
            });
            if (plugins != null && plugins.length > 0){
                ChainProfile chainProfile = new ChainProfile();
                File plugin = plugins[0];
                Toml pluginToml = new Toml().read(plugin);
                chainProfile.setType(pluginToml.getString("common.type"));
                chainProfile.setName(pluginToml.getString("common.name"));
                this.chainProfileList.add(chainProfile);
            }
        }
        return this.chainProfileList;
    }
}
