package link.luyu.sealchain.service;

import java.util.HashMap;
import java.util.Map;

public class PluginContext {
    public static Map<Class<?>, Object> objectMap = new HashMap<>();

    @SuppressWarnings("all")
    public static <T> T getInstance(Class<T> clz){
        Object obj = objectMap.get(clz);
        if(obj == null){
            return null;
        }
        return (T) obj;
    }

    public static void setInstance(Object obj){
        objectMap.putIfAbsent(obj.getClass(), obj);
    }
}
