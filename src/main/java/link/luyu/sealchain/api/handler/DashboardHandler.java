package link.luyu.sealchain.api.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import link.luyu.sealchain.api.Dashboard;
import link.luyu.sealchain.service.PluginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;

public class DashboardHandler implements HttpHandler {
    private static final Logger logger = LoggerFactory.getLogger(DashboardHandler.class);
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final PluginService pluginService;

    public DashboardHandler(PluginService pluginService) {
        this.pluginService = pluginService;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        logger.info("[Plugin] begin handle dashboard request");
        Dashboard dashboard = pluginService.drawDashboard();
        String response = objectMapper.writeValueAsString(dashboard);
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.getBytes(StandardCharsets.UTF_8).length);
        OutputStream responseBody = httpExchange.getResponseBody();
        OutputStreamWriter writer = new OutputStreamWriter(responseBody, StandardCharsets.UTF_8);
        writer.write(response);
        writer.close();
        responseBody.close();
    }
}
