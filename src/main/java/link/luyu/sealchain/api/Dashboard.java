package link.luyu.sealchain.api;

import link.luyu.sealchain.conf.Properties;

import java.util.ArrayList;
import java.util.List;

public class Dashboard {
    private Properties localChainInfo;
    private List<ChainProfile> chainProfiles;
    private ChainState chainState;

    public Dashboard() {
        this.localChainInfo = new Properties();
        this.chainProfiles = new ArrayList<>();
        this.chainState = new ChainState();
    }

    public Dashboard(Properties localChainInfo, List<ChainProfile> chainProfiles, ChainState chainState) {
        this.localChainInfo = localChainInfo;
        this.chainProfiles = chainProfiles;
        this.chainState = chainState;
    }

    public Properties getLocalChainInfo() {
        return localChainInfo;
    }

    public void setLocalChainInfo(Properties localChainInfo) {
        this.localChainInfo = localChainInfo;
    }

    public List<ChainProfile> getChainProfiles() {
        return chainProfiles;
    }

    public void setChainProfiles(List<ChainProfile> chainProfiles) {
        this.chainProfiles = chainProfiles;
    }

    public ChainState getChainState() {
        return chainState;
    }

    public void setChainState(ChainState chainState) {
        this.chainState = chainState;
    }
}
