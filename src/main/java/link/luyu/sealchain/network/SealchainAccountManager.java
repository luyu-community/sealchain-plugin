package link.luyu.sealchain.network;

import link.luyu.protocol.network.Account;
import link.luyu.protocol.network.LuyuSignData;
import link.luyu.protocol.network.AccountManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SealchainAccountManager implements AccountManager {
    private static final Logger logger = LoggerFactory.getLogger(SealchainAccountManager.class);

    @Override
    public Account getAccountBySignature(String signatureType, byte[] luyuSign, LuyuSignData data) {
        logger.trace("[plugin]getAccountBySignature(),signatureType:{};luyuSign:{},data:{}", signatureType, luyuSign.toString(), data.toString());
        return getAccountByIdentity(signatureType, data.getSender());
    }

    @Override
    public Account getAccountByIdentity(String signatureType, String identity) {
        switch (signatureType) {
            default:
                return new SealchainAccount();
        }
    }
}