package link.luyu.sealchain.network;

import link.luyu.protocol.common.STATUS;
import link.luyu.protocol.link.Connection;
import link.luyu.protocol.link.Driver;
import link.luyu.protocol.network.*;
import link.luyu.sealchain.link.SealchainPluginbuilder;
import link.luyu.sealchain.utils.Utils;
import link.luyu.protocol.network.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class SealchainRouter implements RouterManager {
    private static final Logger logger = LoggerFactory.getLogger(SealchainRouter.class);

    private final Map<String, Driver> drivers = new HashMap<>();
    private AccountManager accountManager = new SealchainAccountManager();

    public SealchainRouter() {
        logger.trace("[Plugin]Init SealchainRouter()");
        SealchainPluginbuilder builder = new SealchainPluginbuilder();
        String[] chainPaths = {"payment.fabric1"};
        for (String chainPath : chainPaths) {
            Map<String, Object> connectionConfig = new HashMap<>();
            connectionConfig.put("chainPath", chainPath);
            Connection connection = builder.newConnection(connectionConfig);

            Map<String, Object> driverConfig = new HashMap<>();
            driverConfig.put("chainPath", chainPath);
            Driver driver = builder.newDriver(connection, driverConfig);
        }
    }

    @Override
    public void sendTransaction(Transaction tx, Driver.ReceiptCallback receiptCallback) {
        logger.trace("[Plugin] SealchainRouter SendTransaction():" + tx.toString());
        String chainPath = Utils.getChainPath(tx.getPath());
        Driver driver = drivers.get(chainPath);
        try {
            Account account = accountManager.getAccountBySignature(
                    driver.getSignatureType(), tx.getLuyuSign(), new LuyuSignData(tx)
            );
            driver.sendTransaction(
                    account,
                    tx,
                    new Driver.ReceiptCallback() {
                        @Override
                        public void onResponse(int status, String message, Receipt receipt) {
                            if (status == STATUS.OK) {
                                logger.info("==> Receipt:" + receipt.toString());
                                receiptCallback.onResponse(status, message, receipt);
                            } else {
                                logger.info(message);
                                receiptCallback.onResponse(status, message, receipt);
                            }
                        }
                    });

        } catch (Exception e) {
            logger.info("unexpected exception:", e);
        }
    }

    @Override
    public void call(CallRequest callRequest, Driver.CallResponseCallback callResponseCallback) {
        logger.trace("[Plugin] SealchainRouter call():");
    }

    @Override
    public void getTransactionReceipt(String s, String s1, Driver.ReceiptCallback receiptCallback) {
        logger.trace("[Plugin] SealchainRouter getTransactionReceipt():");
    }

    @Override
    public void getBlockByHash(String s, String s1, Driver.BlockCallback blockCallback) {
        logger.trace("[Plugin] SealchainRouter getBlockByHash():");

    }

    @Override
    public void getBlockByNumber(String s, long l, Driver.BlockCallback blockCallback) {
        logger.trace("[Plugin] SealchainRouter getBlockByNumber():");

    }

    @Override
    public long getBlockNumber(String s) {
        logger.trace("[Plugin] SealchainRouter getBlockNumber():");
        return 0;
    }

    @Override
    public void listResources(String s, Driver.ResourcesCallback resourcesCallback) {
        logger.trace("[Plugin] SealchainRouter listResources():");
    }
}




