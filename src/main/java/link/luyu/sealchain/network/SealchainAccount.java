package link.luyu.sealchain.network;

import link.luyu.protocol.common.STATUS;
import link.luyu.protocol.network.Account;
import link.luyu.sealchain.algorithm.SealchainSignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

public class SealchainAccount implements Account {
    private static final Logger logger = LoggerFactory.getLogger(SealchainAccount.class);

    @Override
    public byte[] getPubKey() {
        return ("SealchainAccount".getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public void sign(byte[] bytes, SignCallback signCallback) {
        logger.trace("[Plugin]SealchainAccount.sign()");
        signCallback.onResponse(STATUS.OK, "Account.sign() Success", queryAccountManagerToSign(bytes));
    }

    @Override
    public void verify(byte[] sign, byte[] message, VerifyCallback verifyCallback) {
        logger.trace("[Plugin]SealchainAccount.verify()");
        verifyCallback.onResponse(STATUS.OK, "Account.verify() Success", queryAccountManagerToVerify(sign, message));
    }

    @Override
    public void setProperty(String s, String s1) {

    }

    @Override
    public String getProperty(String s) {
        return null;
    }

    private byte[] queryAccountManagerToSign(byte[] message) {
        SealchainSignatureAlgorithm algorithm = new SealchainSignatureAlgorithm();
        return algorithm.sign(new byte[]{}, message);
    }

    private boolean queryAccountManagerToVerify(byte[] sign, byte[] message) {
        SealchainSignatureAlgorithm algorithm = new SealchainSignatureAlgorithm();
        return algorithm.verify(getPubKey(), sign, message);
    }

}
