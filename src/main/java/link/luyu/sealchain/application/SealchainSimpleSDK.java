package link.luyu.sealchain.application;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import link.luyu.sealchain.api.handler.DashboardHandler;
import link.luyu.sealchain.application.RequestBody.EventSvcRequest;
import link.luyu.sealchain.application.RequestBody.RpcRequest;
import link.luyu.sealchain.conf.Properties;
import link.luyu.sealchain.service.PluginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class SealchainSimpleSDK {
    private static final Logger logger = LoggerFactory.getLogger(SealchainSimpleSDK.class);
    Properties conf;
    HttpServer server;

    public SealchainSimpleSDK(Properties conf) {
        this.conf = conf;
    }

    public String QueryLastBlockInfo(String channelId, String chainName) {
        try {
            URL url = new URL("http://" + conf.getIp() + ":" + conf.getPort() + "/block");
            String body = RpcRequest.makeBlockQueryReqParams("lastBlock", chainName, channelId, conf.getUser(), conf.getOrg(), false, "");
            return call(url, chainName, body);
        } catch (Exception e) {
            logger.info("call exception", e);
            return "";
        }
    }

    public String QueryBlockByHash(String channelId, String chainName, String hashKey) {
        try {
            URL url = new URL("http://" + conf.getIp() + ":" + conf.getPort() + "/block");
            String body = RpcRequest.makeBlockQueryReqParams("blockInfo", chainName, channelId, conf.getUser(), conf.getOrg(), false, hashKey);
            return call(url, chainName, body);
        } catch (Exception e) {
            logger.info("call exception", e);
            return "";
        }
    }

    // comment by zengzc 20231108:调用玺链的invoke核心函数
    public String ChaincodeInvoke(String channelId, String chaincodeName, String func, String[] params) {
        try {
            URL theurl = new URL("http://" + conf.getIp() + ":" + conf.getPort() + "/chaincode");
            String body = RpcRequest.makeChaincodeRequestBody(channelId, "invoke", params, conf.getUser(),
                    chaincodeName, conf.getOrg(), func, false);
            return call(theurl, conf.getNetworkId(), body);
        } catch (Exception e) {
            logger.info("call exception", e);
            return "";
        }
    }

    // comment by zengzc 20231108:调用玺链的query核心函数
    public String ChaincodeQuery(String channelId, String chaincodeName, String func, String[] params) {
        try {
            URL theurl = new URL("http://" + conf.getIp() + ":" + conf.getPort() + "/chaincode");
            String body = RpcRequest.makeChaincodeRequestBody(channelId, "query", params, conf.getUser(),
                    chaincodeName, conf.getOrg(), func, false);
            return call(theurl, conf.getNetworkId(), body);
        } catch (Exception e) {
            logger.info("call exception", e);
            return "";
        }
    }

    public String GetBlockByNumber(String channelId, String blocknumber) {
        try {
            URL theurl = new URL("http://" + conf.getIp() + ":" + conf.getPort() + "/channel");
            String body = RpcRequest.makeGetBlockBody(channelId, blocknumber, conf.getUser(), conf.getOrg());
            return call(theurl, conf.getNetworkId(), body);
        } catch (Exception e) {
            logger.info("call exception", e);
            return "";
        }
    }

    private String call(URL url, String networkId, String body) throws IOException {
        logger.trace("[Plugin]sdk.call:{},{},{}", url, networkId, body);
        InputStream is = null;
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) (url.openConnection());
            conn.setConnectTimeout(0);
            conn.setReadTimeout(0);
            if (!Objects.equals(networkId, "")) {
                conn.setRequestProperty("network-id", networkId);
            }
            conn.setDoOutput(true);
            conn.getOutputStream().write(body.getBytes(StandardCharsets.UTF_8));
            int rcode = conn.getResponseCode();
            if (200 != rcode) {
                is = conn.getErrorStream();
            } else {
                is = conn.getInputStream();
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len = -1;
            while (-1 != (len = is.read(buffer))) {
                baos.write(buffer, 0, len);
            }
            baos.flush();

            String result = baos.toString("utf-8");
            if (200 != rcode) {
                logger.trace("[Plugin]Error 200!=rcode,result: {}\tbody: {}", result, body);
            }
            return result;
        } catch (SocketTimeoutException e) {
            logger.trace("[Plugin]httpSendMsg SocketTimeoutException:" + e);
            throw new SocketTimeoutException(e.getMessage());
        } catch (IOException e) {
            logger.trace("[Plugin]httpSendMsg IOException:" + e);
            throw new IOException(e.getMessage());
        } finally {
            if (is != null) {
                is.close();
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public void Check() {
        ChaincodeQuery(conf.getChannelId(), conf.getChaincodeName(), "version", new String[]{});
    }


    public void StartListener() throws IOException {
        int serverPort = Integer.parseInt(this.conf.getEventPort());
        this.server = HttpServer.create(new InetSocketAddress("0.0.0.0", serverPort), 0);
        this.server.createContext("/ping", new PingHandler());
        logger.info("[plugin] start listen api.dashboard!");
        this.server.createContext("/api/dashboard", new DashboardHandler(new PluginService()));
        this.server.setExecutor(null);
        this.server.start();
    }

    public void StopListener() {
        this.server.stop(0);
    }

    public void StartEventListener(HttpHandler handler) throws IOException {
        if (this.conf.getEventType().equals("1")) {
            this.server.createContext("/event", handler);
            int serverPort = Integer.parseInt(this.conf.getEventPort());
            String serverUrl = conf.getEventHost() + ":" + conf.getEventPort() + "/event";
            String exposeUrl = conf.getEventHost() + ":" + conf.getEventExposePort() + "/event";
            logger.info("[Plugin]StartEventListener server started!,serverPort:{};serverUrl:{}; exposeUrl:{}", serverPort, serverUrl, exposeUrl);
            URL url = new URL("http://" + conf.getIp() + ":" + conf.getPort() + "/event_svc");
            String body = EventSvcRequest.makeEventSvcRequestBody("addEventSub", conf.getChainName(),
                    conf.getChannelId(), conf.getEventSubUsername(), exposeUrl,
                    conf.getChaincodeName(), "TRANSACTION_EVENT");
            call(url, "baas-network", body);
            logger.info("[Plugin]StartEventListener addEventSub done!");
        }
    }

    private static class PingHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            Headers responseHeaders = httpExchange.getResponseHeaders();
            responseHeaders.set("Content-Type", "text/html;charset=utf-8");
            String response = "[LuyuSealchainPlugin] ok\n";
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.getBytes(StandardCharsets.UTF_8).length);
            OutputStream responseBody = httpExchange.getResponseBody();
            OutputStreamWriter writer = new OutputStreamWriter(responseBody, StandardCharsets.UTF_8);
            writer.write(response);
            writer.close();
            responseBody.close();
        }
    }

}