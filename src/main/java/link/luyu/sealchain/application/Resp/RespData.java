package link.luyu.sealchain.application.Resp;

public class RespData {
    private Long number;
    private String currentBlockHash;
    private String previousBlockHash;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getCurrentBlockHash() {
        return currentBlockHash;
    }

    public void setCurrentBlockHash(String currentBlockHash) {
        this.currentBlockHash = currentBlockHash;
    }

    public String getPreviousBlockHash() {
        return previousBlockHash;
    }

    public void setPreviousBlockHash(String previousBlockHash) {
        this.previousBlockHash = previousBlockHash;
    }

    @Override
    public String toString() {
        return "RespData{" +
                "number=" + number +
                ", currentBlockHash='" + currentBlockHash + '\'' +
                ", previousBlockHash='" + previousBlockHash + '\'' +
                '}';
    }

}