package link.luyu.sealchain.application.Resp;

public class RespResult {
    private String status;
    private String message;
    private String uuid;
    private RespData data;
    private RespError error;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public RespData getData() {
        return data;
    }

    public void setData(RespData data) {
        this.data = data;
    }

    public RespError getError() {
        return error;
    }

    public void setError(RespError error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "RespResult{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", uuid='" + uuid + '\'' +
                ", data=" + data +
                ", error=" + error +
                '}';
    }
}