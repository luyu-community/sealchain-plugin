package link.luyu.sealchain.application.Resp;

public class RespDto {
    private String jsonrpc;
    private RespResult result;

    public String getJsonrpc() {
        return jsonrpc;
    }

    public void setJsonrpc(String jsonrpc) {
        this.jsonrpc = jsonrpc;
    }

    public RespResult getResult() {
        return result;
    }

    public void setResult(RespResult result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "RespDto{" +
                "jsonrpc='" + jsonrpc + '\'' +
                ",result=" + result +
                '}';
    }
}