package link.luyu.sealchain.application.RequestBody;

import com.google.gson.GsonBuilder;

public class EventSvcRequest {
    private String method;
    private SubPayload add_events_sub_payload;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public SubPayload getAdd_events_sub_payload() {
        return add_events_sub_payload;
    }

    public void setAdd_events_sub_payload(SubPayload add_events_sub_payload) {
        this.add_events_sub_payload = add_events_sub_payload;
    }

    public static String makeEventSvcRequestBody(String method, String chainName, String channelId, String name,
                                                 String uri, String ccId, String eventId) {
        EventSvcRequest eventSvcRequest = new EventSvcRequest();
        eventSvcRequest.setMethod(method);

        SubPayload subPayload = new SubPayload();
        subPayload.setChain_name(chainName);
        subPayload.setChannel_id(channelId);
        subPayload.setName(name);
        subPayload.setUri(uri);
        subPayload.setCc_id(ccId);
        subPayload.setEvent_id(eventId);

        eventSvcRequest.setAdd_events_sub_payload(subPayload);
        return new GsonBuilder().disableHtmlEscaping().create().toJson(eventSvcRequest);

    }
}

class SubPayload {
    private String chain_name;
    private String channel_id;
    private String name;
    private String uri;
    private String cc_id;
    private String event_id;

    public String getChain_name() {
        return chain_name;
    }

    public void setChain_name(String chain_name) {
        this.chain_name = chain_name;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getCc_id() {
        return cc_id;
    }

    public void setCc_id(String cc_id) {
        this.cc_id = cc_id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
}