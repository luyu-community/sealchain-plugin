package link.luyu.sealchain.application.RequestBody;

import com.google.gson.GsonBuilder;

public class RpcRequest {
    private String jsonrpc;
    private String method;
    private String mode;
    private RpcParams params;

    public String getJsonrpc() {
        return jsonrpc;
    }

    public void setJsonrpc(String jsonrpc) {
        this.jsonrpc = jsonrpc;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public RpcParams getParams() {
        return params;
    }

    public void setParams(RpcParams params) {
        this.params = params;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public static String makeBlockQueryReqParams(String method, String chainName, String channelId, String username, String orgName, Boolean isUseCA, String searchKey) throws Exception {
        BlockQueryParams params = new BlockQueryParams(method, chainName, channelId, username, orgName, isUseCA, searchKey);
        return new GsonBuilder().disableHtmlEscaping().create().toJson(params);
    }

    public static String makeChaincodeRequestBody(String channelId, String type, String[] params,
                                                  String user, String chaincodeName, String orgName, String func, boolean sync) {
        RpcChaincode chaincode = new RpcChaincode();
        chaincode.setId(chaincodeName);
        chaincode.setType(BlockchainConst.AccessConst.GOLAND);
        chaincode.setArgs(params);
        chaincode.setFunction(func);

        RpcParams rpcParams = new RpcParams();
        rpcParams.setChaincode(chaincode);
        rpcParams.setChannel(new RpcChannel(channelId));
        rpcParams.setUserName(user);
        rpcParams.setOrgName(orgName);

        RpcRequest rpcRequestNew = new RpcRequest();
        rpcRequestNew.setJsonrpc(BlockchainConst.AccessConst.JSONRPC);
        rpcRequestNew.setMethod(type);

        if (sync) {
            rpcRequestNew.setMode(BlockchainConst.AccessConst.SYNC_MODE);
        } else {
            rpcRequestNew.setMode(BlockchainConst.AccessConst.ASYNC_MODE);
        }
        rpcRequestNew.setParams(rpcParams);
        return new GsonBuilder().disableHtmlEscaping().create().toJson(rpcRequestNew);
    }

    public static String makeGetBlockBody(String channelId, String blockNumber, String User, String orgName) {
        RpcChannel channel = new RpcChannel(channelId);
        channel.setFunction("queryBlock");
        channel.setBlockNumber(blockNumber);
        RpcParams rpcParams = new RpcParams();
        rpcParams.setChannel(channel);
        rpcParams.setUserName(User);
        rpcParams.setOrgName(orgName);
        RpcRequest rpcRequestNew = new RpcRequest();
        rpcRequestNew.setJsonrpc(BlockchainConst.AccessConst.JSONRPC);
        rpcRequestNew.setParams(rpcParams);
        return new GsonBuilder().disableHtmlEscaping().create().toJson(rpcRequestNew);
    }
}