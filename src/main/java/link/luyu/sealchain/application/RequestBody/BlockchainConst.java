package link.luyu.sealchain.application.RequestBody;

public class BlockchainConst {
    public static final int DEFAULT_SYNC_TIMEOUT = 30000;

    public static class RespConst {
        public static final String OUTPUT_BLOCKCHAIN_MESSAGE = "blockchainMessage";
        public static final String OUTPUT_BLOCKCHAIN_UUID = "blockchainUuid";
        public static final String OUTPUT_BLOCKCHAIN_SUCCESS = "blockchainSuccess";
        public static final String OUTPUT_BLOCKCHAIN_STATUS = "blockchainStatus";
        public static final String OUTPUT_BLOCKCHAIN_RESULT = "blockchainResponse";
    }

    public static class ResultStatus {
        public static final int STATUS_NORMAL = 0;
        public static final int STATUS_FAILURE = 1;
        public static final int STATUS_TIMEOUT = 2;
    }

    public static class AccessConst {
        public static final int ID = 1;
        public static final int GOLAND = 1;
        public static final String JSONRPC = "2.0";
        public static final String INVOKE_TYPE = "invoke";
        public static final String QUERY_TYPE = "query";
        public static final String SYNC_MODE = "sync";
        public static final String ASYNC_MODE = "async";
        public static final String HTTP_MODE = "http";
        public static final String HTTP_PROTOCOL = "http://";
        public static final String HTTPS_PROTOCOL = "https://";
        public static final String HTTPS_1_MODE = "https_1";
        public static final String HTTPS_2_MODE = "https_2";
        public static final int DEFAULT_TIME_OUT = 15000;
    }

    public static class ResultMessageStatus {
        public static final String STATUS_NORMAL = "0";
        public static final String STATUS_FAILURE = "1";
        public static final String STATUS_TIMEOUT = "2";
    }

    public static class RSAConst {
        public static final String KEY_ALGORITHM = "RSA";
        public static final String CIPHER_ALGORITHM = "RSA/ECB/PKCS1Padding";
        public static final String PUBLIC_KEY = "publicKey";
        public static final String PRIVATE_KEY = "privateKey";
        public static final int KEY_SIZE = 2048;
        public static final Long KEY_TIME = 3300L;
        public static final int MAX_ENCRYPT_BLOCK = 245;
        public static final String NVP_PUBLIC_KEY = "nvpPublicKey";
    }
}