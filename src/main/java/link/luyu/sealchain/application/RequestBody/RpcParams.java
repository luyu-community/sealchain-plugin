package link.luyu.sealchain.application.RequestBody;

public class RpcParams {
    private String userName;
    private String orgName;
    private RpcChannel channel;
    private RpcChaincode chaincode;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public RpcChannel getChannel() {
        return channel;
    }

    public void setChannel(RpcChannel channel) {
        this.channel = channel;
    }

    public RpcChaincode getChaincode() {
        return chaincode;
    }

    public void setChaincode(RpcChaincode chaincode) {
        this.chaincode = chaincode;
    }
}