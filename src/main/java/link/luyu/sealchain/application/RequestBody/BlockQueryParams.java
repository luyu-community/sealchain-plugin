package link.luyu.sealchain.application.RequestBody;

public class BlockQueryParams {
    private String method;
    private BlockQueryLastTenPayload last_ten_payload;
    private BlockQueryPayload query_last_block_payload;
    private BlockQueryInfoPayload query_info_payload;

    BlockQueryParams(String method, String chainName, String channelId, String username, String orgName, Boolean isUseCA, String searchKey) throws Exception {
        this.method = method;
        switch (method) {
            case "lastTenBlock":
                this.last_ten_payload = new BlockQueryLastTenPayload();
                this.last_ten_payload.setChain_name(chainName);
                this.last_ten_payload.setChannelId(channelId);
                this.last_ten_payload.setUsername(username);
                this.last_ten_payload.setUseCA(isUseCA);
                this.last_ten_payload.setOrg_name(orgName);
                break;
            case "lastBlock":
                this.query_last_block_payload = new BlockQueryPayload();
                this.query_last_block_payload.setChain_name(chainName);
                this.query_last_block_payload.setChannelId(channelId);
                this.query_last_block_payload.setUsername(username);
                this.query_last_block_payload.setUseCA(isUseCA);
                this.query_last_block_payload.setOrg_name(orgName);
                break;
            case "blockInfo":
                this.query_info_payload = new BlockQueryInfoPayload();
                this.query_info_payload.setChain_name(chainName);
                this.query_info_payload.setChannelId(channelId);
                this.query_info_payload.setUsername(username);
                this.query_info_payload.setUseCA(isUseCA);
                this.query_info_payload.setOrg_name(orgName);
                this.query_info_payload.setSearch(searchKey);
                break;
            default:
                throw new Exception("unsupport method:" + method);

        }
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public BlockQueryLastTenPayload getLast_ten_payload() {
        return last_ten_payload;
    }

    public void setLast_ten_payload(BlockQueryLastTenPayload last_ten_payload) {
        this.last_ten_payload = last_ten_payload;
    }

    public BlockQueryPayload getQuery_last_block_payload() {
        return query_last_block_payload;
    }

    public void setQuery_last_block_payload(BlockQueryPayload query_last_block_payload) {
        this.query_last_block_payload = query_last_block_payload;
    }

    public BlockQueryInfoPayload getQuery_info_payload() {
        return query_info_payload;
    }

    public void setQuery_info_payload(BlockQueryInfoPayload query_info_payload) {
        this.query_info_payload = query_info_payload;
    }
}

class BlockQueryLastTenPayload {
    private String chain_name;
    private String channelId;
    private String username;
    private String org_name;
    private boolean isUseCA;

    public String getChain_name() {
        return chain_name;
    }

    public void setChain_name(String chain_name) {
        this.chain_name = chain_name;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public boolean isUseCA() {
        return isUseCA;
    }

    public void setUseCA(boolean useCA) {
        isUseCA = useCA;
    }
}

class BlockQueryPayload {
    private String chain_name;
    private String channel_id;
    private String username;
    private String org_name;
    private boolean isUseCA;

    public String getChain_name() {
        return chain_name;
    }

    public void setChain_name(String chain_name) {
        this.chain_name = chain_name;
    }

    public String getChannelId() {
        return channel_id;
    }

    public void setChannelId(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrgName() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public boolean isUseCA() {
        return isUseCA;
    }

    public void setUseCA(boolean useCA) {
        isUseCA = useCA;
    }
}

class BlockQueryInfoPayload {
    private String chain_name;
    private String channel_id;
    private String username;
    private String org_name;
    private boolean isUseCA;
    private String search;

    public String getChannelId() {
        return channel_id;
    }

    public void setChannelId(String channelId) {
        this.channel_id = channelId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public boolean isUseCA() {
        return isUseCA;
    }

    public void setUseCA(boolean useCA) {
        isUseCA = useCA;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getChain_name() {
        return chain_name;
    }

    public void setChain_name(String chain_name) {
        this.chain_name = chain_name;
    }
}

