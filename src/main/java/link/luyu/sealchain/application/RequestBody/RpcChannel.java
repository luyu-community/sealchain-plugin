package link.luyu.sealchain.application.RequestBody;

public class RpcChannel {
    private String id;
    private String function;
    private String blockNumber;

    public RpcChannel(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFunction(String func) {
        this.function = func;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }
}


