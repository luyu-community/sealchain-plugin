package link.luyu.sealchain.event;

import link.luyu.protocol.network.Transaction;

import java.util.Arrays;

public class Event {

    private String path;
    private String txid;
    private String method;
    private String sender;
    private String[] args;

    public Transaction toTransaction() {
        Transaction t = new Transaction();
        t.setPath(this.path);
        t.setMethod(this.method);
        t.setSender(this.sender);
        t.setArgs(this.args);
        t.getProperties().put("txId", this.getTxid());
        return t;
    }

    public String getPath() {
        return path;
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getMethod() {
        return method;
    }

    public String getSender() {
        return sender;
    }

    public String[] getArgs() {
        return args;
    }

    @Override
    public String toString() {
        return "Event{" +
                "path='" + path + '\'' +
                ",method='" + method + '\'' +
                ",sender='" + sender + '\'' +
                ",args=" + Arrays.toString(args) +
                '}';
    }

}