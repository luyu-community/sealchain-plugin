package link.luyu.sealchain.utils;

import link.luyu.protocol.network.Transaction;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class Utils {

    public static byte[] bytesConcat(byte[] a, byte[] b) {
        byte[] res = new byte[a.length + b.length];
        System.arraycopy(a, 0, res, 0, a.length);
        System.arraycopy(b, 0, res, a.length, b.length);
        return res;
    }

    public static String getChainPath(String path) {
        String[] sp = path.split("\\.");
        return sp[0] + "." + sp[1];
    }

    public static String getResourceName(String path) {
        String[] sp = path.split("\\.");
        return sp[2];
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(bytes);
        buffer.flip();
        return buffer.getLong();
    }

    private static final String tx_delimiter = new String(new char[]{0x01});

    public static byte[] txToTxbytes(Transaction tx) {
        String func = tx.getMethod();
        String[] args = tx.getArgs();
        String[] out = new String[args.length + 1];
        System.arraycopy(args, 0, out, 1, args.length);
        out[0] = func;
        return String.join(tx_delimiter, out).getBytes(StandardCharsets.UTF_8);
    }

    public static String[] txBytesToArr(byte[] txbytes) {
        String txStr = new String(txbytes);
        return txStr.split(tx_delimiter);
    }
}
