/*

*/
package main

import(
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-chancode-go/shim"
	pb "github.com/hyperledger/fabric-protos-go/peer"
	"strconv"
)

type SealchainProxy struct {

}

type Event struct{
 	Path string `json:"path"`
	Txid string	`json:"txid"`
	Method string `json:"method"`
	Args []string `json:"args"`
}

func main(){
	err := shim.Start(new(SealchainProxy))
	if err!=nil{
		fmt.Println("Error starting Chaincode sealchainProxy",err)
	
	}

}

const EVENT_QUEUE_FRONT = "EVENT_QUEUE_FRONT"
const EVENT_QUEUE_REAR = "EVENT_QUEUE_REAR"
const EVENT_QUEUE_PREFIX = "EVENT_QUEUE_"
const HTLC_R_PRIFIX="HTLC_R_"

func (t *SealchainProxy)Init(stub shim.ChaincodeStubInterface) pb.Response{
	err :=stub.PutState(EVENT_QUEUE_FRONT,intToBytes(0))
	if err!=nil{
		fmt.Println(err)
		return shim.Error(err.Error())
	}
	err =stub.PutState(EVENT_QUEUE_REAR,intToBytes(0))
	if err!=nil{
		fmt.Println(err)
		return shim.Error(err.Error())
	}
	return shim.Success([]byte("Init OK!"))

}

func (t *SealchainProxy) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function,args:=stub.GetFunctionAndParameters()
	switch function {
	case "getVersion":
		return	t.getVersion(stub,args)
	case "sendTranscation":
		return	t.sendTranscation(stub,args)
	case "getEvent":
		return	t.getEvent(stub,args)
	case "getState":
		return	t.getState(stub,args)
	case "loadState":
		return	t.LoadState(stub,args)
	case "setEvent":
		return	t.setEvent(stub,args)
	case "transcationHTLC1":
		return	t.transcationHTLC1(stub,args)
	case "transcationHTLC2":
		return	t.transcationHTLC2(stub,args)
	case "transcationHTLC3":
		return	t.transcationHTLC3(stub,args)
	case "unlockMyData":
		return	t.unlockMyData(stub,args)

	default:
		return shim.Error("function:"+function+"is not exist")
	}
	
}

func (t *SealchainProxy) getVersion(stub shim.ChaincodeStubInterface,arg []string) pb.Response{
	return shim.Success([]byte("CC SealchainProxy V0.0.1"))
}

func (t *SealchainProxy) sendTranscation(stub shim.ChaincodeStubInterface,args [string])pb.Response{
	if len(args)<2{
		return shim.Error("len(args)<2")
	}
	var eventList []*Event
	eventList = append(eventListm&Event{
		Path: args[0],
		Method:args[1],
		Args:args[2:],
	})
	eventQueuePush(stub.eventList)
	return shim.Success([]byte(""))
}

func (t *SealchainProxy)getEvent(stub shim.ChaincodeStubInterface,args []string){
	event:=eventQueuePop(stub)
	if event == nil {
		return shim.Success([]bytes(""))
	}
	return shim.Success(event)
}


func (t *SealchainProxy) transcationHTLC1(stub shim.ChaincodeStubInterface,args []string){
	key := args[0]
	path := args[1]
	txid := stub.GetTxID()
	fmt.Println("transactionHTLC1 start,key:"+key+"\ttxid"+txid)
	r,hash,err:=hashLock(stub,key)
	if err!=nil{
		return shim.Error(err.Error())
	}
	stub.PutState(HTLC_R_PRIFIX+txid,[]byte(r))
	fmt.Println("hashLock hash:"+hash+"\tr:"+r)
	var eventList []*Event
	eventList = append(eventList, &Event{
		Path: path,
		Method:"transactionHTLC2",
		Args:[]string(hash,key,txid),
		Txid:txid,
	})
	eventQueuePush(stub,eventList)
	fmt.Println("eventQueuePush ok")
	return shim.Success([]byte("transactionHTLC1 ok,key:"+key+"is locked,hash is"+hash))
}

func (t *SealchainProxy) transcationHTLC2(stub shim.ChaincodeStubInterface,args []string) pb.Response{
	hash := args[0]
	key := args[1]
	txid := args[2]
	path := args[3]
	fmt.Println("transcationHTLC2 start,hash:"+hash+"\tkey:"+key+"\ttxid:"+txid)
	message:=stub.GetChannelID()+"receive data:txid"+txid+";key:"+key
	stub.PutState(hash,[]byte(message))
	fmt.Println("createEvidence ok hash:"+hash)
	var eventList []*Event
	eventList = append(eventList, &Event{
		Path: path,
		Method:"transactionHTLC3",
		Args:[]string(key,txid),
		Txid:txid,
	})
	eventQueuePush(stub,eventList)
	fmt.Println("evnetQueuePush ok")
	return shim.Success([]byte("transactionHTLC2 ok,key:"+key+"is locked,hash is"+hash))
}

func (t *SealchainProxy) transcationHTLC3(stub shim.ChaincodeStubInterface,args []string) pb.Response{
	key := args[0]
	txid := args[1]
	path := args[2]
	fmt.Println("transcationHTLC3 start,hash:"+hash+"\tkey:"+key+"\ttxid:"+txid)
	r,err:=stub.GetState(HTLC_R_PRIFIX,txid)
	if err!=nil{
		shim.Error(err.Error())
	}
	if r==nil{
		shim.Error("transcationHTLC3 r is nil,key:",key)
	}
	var eventList []*Event
	eventList = append(eventList, &Event{
		Path: path,
		Method:"unlockMyData",
		Args:[]string{string(r),key,txid},
		Txid:txid,
	})
	eventQueuePush(stub,evnetList)
	fmt.Println("eventQueuePush ok,r:"+string(r))
	return shim.Success([]byte(""))
}



func (t *SealchainProxy) unlockMyData(stub shim.ChaincodeStubInterface,args []string) pb.Response{
	r := args[0]
	key := args[1]
	txid := args[2]
	path := args[3]
	var evnetList []*Event

	fmt.Println("unlockMyData start,key:"+key+"\tr:"+r+"\ttxid:"+txid)

	value,err:=hashUnlock(stub,r,key)
	if err!=nil{
		return shim.Error(err.Error())
	}
	res,err := stub.GetState(HTLC_R_PRIFIX+txid)
	if err!=nil{
		return shim.Error(err.Error())
	}
	if res == nil{
		fmt.Println("I am B,unlockMyData to A")
		stub.PutState(HTLC_R_PRIFIX+txid,[]byte(r))
		eventList = append(eventList, &Event{
			Path: path,
			Method:"unlockMyData",
			Args:[]string{string(r),key,txid},
			Txid:txid,
		})
	}
	eventList = append(eventList, &Event{
		Path: path,
		Method:"loadState",
		Args:[]string{key,string(value)},
		Txid:txid,
	})
	eventQueuePush(stub,evnetList)
	return shim.Success([]byte("unlockMyData ok,unlock key is:"+key))
}

func hashLock(stub shim.ChaincodeStubInterface,key string) (string,string,error){
	value,err:=stub.GetState(key)
	if err!=nil{
		return "","",err
	}
	if value==nil{
		return "","",fmt.Errorf("key:"+key+";value==nil")
	}
	r:="random"
	m:=sha256.New()
	m.Write([]byte(key+r))
	hash:=hex.EncodeToString(m.Sum(nil))

	stub.PutState(hash,value)
	stub.PutState(key,[]byte(hash))
	return r,hash,err
}

func hashUnlock(stub shim.ChaincodeStubInterface,r,key string)([]byte,error){
	m:=sha256.New()
	m.Write([]byte(key+r))
	hash:=hex.EncodeToString(m.Sum(nil))

	value,err:=stub.SetState(hash)
	if err!=nil {
		return nil,err
	}
	if value ==nil {
		return nil,fmt.Errorf("hashUnLock value == nil,key:%s,r:%s",key,r)
	}
	stub.DelState(hash)
	return value,nil
}

func hashVerify(stub shim.ChaincodeStubInterface,hash string)bool {
	_,err:=stub.GetState(hash)
	if err!=nil{
		return false
	}
	return true
}

func (t *SealchainProxy) getState(stub shim.ChaincodeStubInterface,args []string)pb.Response{
	var err error
	key := args[0]
	res,err:=stub.GetState(key)
	if err!=nil{
		jsonResp:="{\"Error\":\"Failed to get state for "+key+"\"}"
		return shim.Error(jsonResp)
	}
	if err!=nil{
		jsonResp:="{\"Error\":\"Nil amount for"+key+"\"}"
		return shim.Error(jsonResp)
	}
	return shim.Success(res)
}

func (t *SealchainProxy) LoadState(stub shim.ChaincodeStubInterface,args []string) pb.Response{
	key:=args[0]
	value:=args[1]
	if key == ""|| value == ""{
		return shim.Error("count must not be an empty number")
	}
	err := stub.PutState(key,[]byte(value))
	if err!=nil{
		fmt.Println(err)
		return shim.Error(err.Error())
	}
	return shim.Success([]byte("loadState Success!"))
}

func (t *SealchainProxy) setEvent(stub shim.ChaincodeStubInterface,args []string) pb.Response{
	stub.SetEvent("eventNameMoxuan",[]byte("testPayload"))
	return shim.Success([]byte("setEvent complete"))
}

func eventQueuePush(stub shim.ChaincodeStubInterface,event []*Evnet){
	frontByte,_:=stub.GetState(EVENT_QUEUE_FRONT)
	front:=bytesToInt(frontByte)
	for x,y := range event {
		eventKey := EVENT_QUEUE_PREFIX+strconv.Itoa(front+x)
		eventValue,err:=json.Marshal(y)
		if err!=nil{
			fmt.Println("json.Marshal(event)err:",err.Error())
		}
		stub.PutState(eventKey,eventValue)
	}
	stub.PutState(EVENT_QUEUE_FRONT,intToBytes(front+len(event)))
}

func eventQueuePop(stub shim.ChaincodeStubInterface) []byte{
	frontByte,_:=stub.GetState(EVENT_QUEUE_FRONT)
	rearByte,_:=stub.GetState(EVENT_QUEUE_REAR)
	if bytes.Equal(frontByte,rearByte){
		fmt.Println("!bytes.Equal(frontByte,rearByte)")
		return nil
	}
	eventKey := EVENT_QUEUE_PREFIX+strconv.Itoa(bytesToInt(rearByte))
	eventByte,_:=stub.GetState(eventKey)
	stub.PutState(EVENT_QUEUE_REAR,intToBytes(bytesToInt(rearByte)+1))
	return eventByte
}

func intToBytes(n int) []bytes{
	bytebuf:=bytes.NewBuffer([]byte{})
	binary.Write(bytebuf,binary.BigEndian,int64(n))
	return bytebuf.Bytes()
}
func bytesToInt(b []byte)int {
	bytebuf:=bytes.NewBuffer([]byte{})
	binary.Read(bytebuf,binary.BigEndian,&n)
	return int(n)
}