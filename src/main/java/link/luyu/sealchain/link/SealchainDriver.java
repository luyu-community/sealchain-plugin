package link.luyu.sealchain.link;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import link.luyu.protocol.algorithm.ecdsa.secp256k1.EcdsaSecp256k1WithSHA256;
import link.luyu.protocol.common.STATUS;
import link.luyu.protocol.link.Connection;
import link.luyu.protocol.link.Driver;
import link.luyu.protocol.network.*;
import link.luyu.sealchain.application.Resp.RespDto;
import link.luyu.sealchain.event.Event;
import link.luyu.sealchain.event.RestEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import link.luyu.sealchain.utils.Utils;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.CountDownLatch;

public class SealchainDriver implements Driver {
    private static final Logger logger = LoggerFactory.getLogger(SealchainDriver.class);

    private Connection connection;
    private Map<String, Resource> resource = new HashMap<>();
    private Events routerEventsHandler;
    private Map<String, Object> conf;
    private Random random = new SecureRandom();
    private String chainPath;

    public SealchainDriver(Connection connection, Map<String, Object> properties) {
        logger.info("[Plugin] Init SealchainDriver() properties:" + properties);
        this.connection = connection;
        this.conf = properties;
        this.chainPath = (String) conf.get("chainPath");
    }

    @Override
    public void start() throws RuntimeException {
        logger.info("[Plugin]SealchainDriver.start()");
        connection.subscribe(0, new byte[0], new Connection.Callback() {
            @Override
            public void onResponse(int errCode, String strRestEvent, byte[] bytes) {
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                RestEvent restEvent = gson.fromJson(strRestEvent, RestEvent.class);
                String strEvent = new String(Base64.getDecoder().decode(restEvent.getPayload()));
                Event event = gson.fromJson(strEvent, Event.class);
                logger.info("[Plugin]SealchainDriver txId:{} strEvent:{}", restEvent.getTx_id(), strEvent);
                event.setTxid(restEvent.getTx_id());
                processEvent(event);
            }
        });

    }

    @Override
    public void stop() throws RuntimeException {
        logger.info("[Plugin]SealchainDriver.stop()");
    }

    @Override
    public void sendTransaction(Account account, Transaction tx, ReceiptCallback receiptCallback) {
        byte[] byteTx = Utils.txToTxbytes(tx);
        logger.info("[Plugin]SealchainDriver.sendTransaction(),Account:{},Transaction:{},byteTx:{}", account.toString(), tx.toString(), new String(byteTx));
        connection.asyncSend("", SealchainConnection.SEND_TRANSCATION, byteTx, new Connection.Callback() {
            @Override
            public void onResponse(int errCode, String message, byte[] responseData) {
                if (errCode == STATUS.OK) {
                    Receipt receipt = new Receipt();
                    receipt.setResult(new String[]{new String(responseData)});
                    receiptCallback.onResponse(STATUS.OK, message, receipt);
                } else {
                    receiptCallback.onResponse(STATUS.INTERNAL_ERROR, "", null);
                }
            }
        });
    }

    @Override
    public void call(Account account, CallRequest callRequest, CallResponseCallback callResponseCallback) {
        logger.trace("[Plugin]SealchainDriver.call()");
    }

    @Override
    public void getTransactionReceipt(String s, ReceiptCallback receiptCallback) {
        logger.trace("[Plugin]SealchainDriver.getTransactionReceipt()");
    }

    @Override
    public void getBlockByHash(String s, BlockCallback blockCallback) {
        logger.trace("[Plugin]SealchainDriver.getBlockByHash(),hash:{}", s);
        connection.asyncSend("", SealchainConnection.GET_BLOCK_BY_HASH, s.getBytes(StandardCharsets.UTF_8), new Connection.Callback() {
            @Override
            public void onResponse(int errCode, String message, byte[] responseData) {
                if (errCode == STATUS.OK) {
                    RespDto respDto = new GsonBuilder().disableHtmlEscaping().create().fromJson(Arrays.toString(responseData), RespDto.class);
                    logger.info("GET_BLOCK_BY_HASH respDto:{}",respDto.toString());
                    if (respDto == null) {
                        blockCallback.onResponse(STATUS.INTERNAL_ERROR, "respDto is null", null);
                        return;
                    }
                    Block block = new Block();
                    block.setNumber(respDto.getResult().getData().getNumber());
                    block.setHash(respDto.getResult().getData().getCurrentBlockHash());
                    blockCallback.onResponse(STATUS.OK, message, block);
                } else {
                    blockCallback.onResponse(STATUS.INTERNAL_ERROR, "", null);
                }
            }
        });
    }

    @Override
    public void getBlockByNumber(long blockNumber, BlockCallback blockCallback) {
        logger.trace("[Plugin]SealchainDriver.getBlockByNumber(),blockNumber:{}", blockNumber);
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(blockNumber);
        connection.asyncSend("", SealchainConnection.GET_BLOCK_BY_NUMBER, buffer.array(), new Connection.Callback() {
            @Override
            public void onResponse(int errCode, String message, byte[] responseData) {
                if (errCode == STATUS.OK) {
                    Block block = new Block();
                    block.setNumber(1);
                    block.setBytes(responseData);
                    block.setParentHash(new String[]{"aaabbbcccddd"});
                    block.setHash("0xtestblockHash");
                    blockCallback.onResponse(STATUS.OK, message, block);
                }
            }
        });
    }

    @Override
    public long getBlockNumber() {
        BlockNumberResp blockNumberResp = new BlockNumberResp();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        final long reqId = System.currentTimeMillis() + random.nextInt(10000);

        logger.debug("[Plugin]SealchainDriver.getBlockNumber() connection={} reqId={}", connection.getClass(), reqId);
        connection.asyncSend("", SealchainConnection.GET_BLOCK_NUMBER, null, new Connection.Callback() {
            @Override
            public void onResponse(int errCode, String message, byte[] responseData) {
                if (errCode == STATUS.OK) {
                    RespDto respDto = new GsonBuilder().disableHtmlEscaping().create().fromJson(new String(responseData), RespDto.class);
                    logger.debug("[Plugin] reqId={} response={}", reqId, respDto);
                    if (respDto != null && respDto.getResult() != null && respDto.getResult().getData() != null) {
                        blockNumberResp.setNumber(respDto.getResult().getData().getNumber());
                    }
                }
                countDownLatch.countDown();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            logger.warn("unexpected exception", e);
        }
        return blockNumberResp.getNumber();
    }

    @Override
    public String getType() {
        logger.trace("[Plugin]SealchainDriver.getType()");
        return "Sealchain";
    }

    @Override
    public String getSignatureType() {
        logger.trace("[Plugin]SealchainDriver.getSignatureType()");
        return EcdsaSecp256k1WithSHA256.TYPE;
    }

    @Override
    public void listResources(ResourcesCallback resourcesCallback) {
        logger.trace("[Plugin]SealchainDriver.listResources()");
        Resource r = new Resource();
        r.setPath(this.chainPath);
        r.setType("Sealchain");
        r.setMethods(new String[]{"getState", "loadState"});
        logger.trace("[Plugin]Resources:{}", r.toString());
        resourcesCallback.onResponse(STATUS.OK, "listResources() success", new Resource[]{r});
    }

    @Override
    public void registerEvents(Events events) {
        logger.trace("[Plugin]SealchainDriver.registerEvents()");
        this.routerEventsHandler = events;
    }

    public Events getRouterEventsHandler() {
        return routerEventsHandler;
    }

    private void processEvent(Event event) {
        Transaction tx = event.toTransaction();
        logger.info("[Plugin]发起事件调用tx:{}", tx.toString());
        switch (tx.getMethod()) {
            case "transactionHTLC2":
            case "transactionHTLC3":
            case "unlockMyData":
                logger.info("[Plugin] call {}, add path: {}", tx.getMethod(), this.chainPath);
                List<String> list = new ArrayList<>(Arrays.asList(tx.getArgs()));
                list.add(this.chainPath);
                tx.setArgs(list.toArray(new String[0]));
                logger.info("[Plugin] add path ok!");
                break;
            default:
                break;
        }
        this.routerEventsHandler.sendTransaction(tx,
                new Driver.ReceiptCallback() {
                    @Override
                    public void onResponse(int errCode, String message, Receipt responseData) {
                        if (errCode == STATUS.OK) {
                            logger.info("[Plugin]call sendTransaction() successful\nmessage:{}\nresponseData:{}", message, responseData);
                        } else {
                            logger.error("[Plugin]call sendTransaction() failed!\nmessage:{}\nresponseData:{}", message, responseData);
                        }
                    }
                }
        );

    }
}

class BlockNumberResp {
    private long number;

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }
}

