package link.luyu.sealchain.link;

import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import link.luyu.protocol.common.STATUS;
import link.luyu.protocol.link.Connection;
import link.luyu.sealchain.application.Resp.RespDto;
import link.luyu.sealchain.application.SealchainSimpleSDK;
import link.luyu.sealchain.conf.Properties;
import link.luyu.sealchain.event.Event;
import link.luyu.sealchain.service.PluginContext;
import link.luyu.sealchain.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;

public class SealchainConnection implements Connection {
    private static final Logger logger = LoggerFactory.getLogger(SealchainConnection.class);
    public static final int SEND_TRANSCATION = 101;
    public static final int CALL = 102;
    public static final int GET_TRANSCATION_RECEIPT = 103;
    public static final int GET_BLOCK_BY_HASH = 104;
    public static final int GET_BLOCK_BY_NUMBER = 105;
    public static final int GET_BLOCK_NUMBER = 106;

    private SealchainSimpleSDK sdk;
    private Properties conf;
    private Thread heartBeat;

    public SealchainConnection(Map<String, Object> properties) {
        conf = new Properties(properties);
        PluginContext.setInstance(conf);
        logger.info("[Plugin] Init SealchainConnection properties:{}", conf.toString());
        this.sdk = new SealchainSimpleSDK(conf);
    }

    @Override
    public void start() throws RuntimeException {
        try {
            this.sdk.StartListener();
        } catch (IOException e) {
            logger.error("[Plugin]SealchainConnection.start IOException:", e);
        }
        logger.info("[Plugin]SealchainConnection.start()");
    }


    @Override
    public void stop() throws RuntimeException {
        sdk.StopListener();
        logger.trace("[Plugin]SealchainConnection.stop()");
    }


    @Override
    public void asyncSend(String path, int type, byte[] bytes, Callback callback) {
        logger.trace("[Plugin]SealchainConnection.asyncSend()");
        switch (type) {
            case SEND_TRANSCATION: {
                String[] txArr = Utils.txBytesToArr(bytes);
                String func = txArr[0];
                String[] params = Arrays.copyOfRange(txArr, 1, txArr.length);
                logger.info("[Plugin]SealchainConnection.asyncSend()SEND_TRANSCATION,func:{};params:{}", func, params);
                String result = sdk.ChaincodeInvoke(conf.getChannelId(), conf.getChaincodeName(), func, params);
                callback.onResponse(STATUS.OK, "asyncSend.SEND_TRANSCATION Success", result.getBytes(StandardCharsets.UTF_8));
                logger.info("[Plugin]SealchainConnection.asyncSend().SEND_TRANSCATIONDONE!!! result:{}", result);
                break;
            }
            case GET_BLOCK_BY_NUMBER: {
                ByteBuffer buffer = ByteBuffer.wrap(bytes);
                String blockNumber = String.valueOf(buffer.getLong());
                logger.trace("[Plugin]SealchainConnection.asyncSend().GETBLOCKBYNUMBER, blockNumber:{}", blockNumber);
                String block = sdk.GetBlockByNumber(conf.getChannelId(), blockNumber);
                callback.onResponse(STATUS.OK, "asyncSend.GETBLOCKBYNUMBER Success", block.getBytes(StandardCharsets.UTF_8));
                break;
            }
            case GET_BLOCK_BY_HASH: {
                String hash = new String(bytes);
                logger.trace("[Plugin]SealchainConnection.asyncSend().GETBLOCKBYHASH\thash:{}", hash);
                String result = sdk.QueryBlockByHash(conf.getChannelId(), conf.getNetworkId(), Arrays.toString(bytes));
                logger.trace("GET_BLOCK_BY_HASH result:" + result);
                callback.onResponse(STATUS.OK, "asyncSend.GETBLOCKBYHASH success", result.getBytes(StandardCharsets.UTF_8));
                break;
            }
            case GET_BLOCK_NUMBER: {
                logger.trace("[Plugin]SealchainConnection.asyncSend().GETBLOCKNUMBER");
                String result = sdk.QueryLastBlockInfo(conf.getChannelId(), conf.getNetworkId());
                callback.onResponse(STATUS.OK, "asyncSend.GETBLOCKNUMBER success", result.getBytes(StandardCharsets.UTF_8));
                break;
            }
            case CALL: {
                logger.trace("[Plugin]SealchainConnection.asyncSend().call");
                break;
            }
            default: {
                logger.trace("[Plugin]SealchainConnection.asyncSend().default");
                break;
            }
        }
    }

    @Override
    public void subscribe(int i, byte[] bytes, Callback callback) {
        logger.info("[Plugin]SealchainConnection.subscribe()");
        if (this.conf.getEventEnabled()) {
            if (this.conf.getEventType().equals("1")) {
                logger.info("[Plugin] eventType == 1,StartEventListener");
                try {
                    this.sdk.StartEventListener(new EventHandler(callback));
                } catch (Exception e) {
                    logger.info("[Plugin]EventListener error", e);
                    System.exit(1);
                }
            } else {
                logger.info("[Plugin] eventType != 1; Start heartbeatLoop");
                heartBeat = new Thread(
                        () -> {
                            while (true) {
                                try {
                                    Thread.sleep(1000);
                                    String strEvent = heartBeatLoop();
                                    if (!strEvent.isEmpty()) {
                                        callback.onResponse(0, strEvent, new byte[0]);
                                    }
                                } catch (Exception e) {
                                    logger.error("[Plugin]heartBeat err! e: {}", e.toString());
                                }
                            }
                        }
                );
                heartBeat.start();
            }
        }
    }

    public Properties getConf() {
        return this.conf;
    }

    public String getChainEvent(){
        return heartBeatLoop();
    }

    private String heartBeatLoop() {
        String func = "getEvent";
        String[] params = {""};
        String result = sdk.ChaincodeInvoke(conf.getChannelId(), conf.getChaincodeName(), func, params);
        RespDto respDto = new GsonBuilder().disableHtmlEscaping().create().fromJson(result, RespDto.class);
        logger.trace("[Plugin]heartBeatLoop:{}", respDto.toString());

        if (respDto.getResult().getStatus().equals("OK") && respDto.getResult().getMessage() != null) {
            logger.info("[Plugin]heartBeatLoop() 发现事件!{} \t from:{}", respDto.getResult().getMessage(), conf.getChainPath());
            return respDto.getResult().getMessage();
        }
        return "";
    }


    private class EventHandler implements HttpHandler {
        private Callback callback;

        public EventHandler(Callback cb) {
            this.callback = cb;
        }

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            Headers responseHeaders = httpExchange.getResponseHeaders();
            responseHeaders.set("Content-Type", "text/html;charset=utf-8");

            InputStream requestBody = httpExchange.getRequestBody();
            int count = 0;
            while (count == 0) {
                count = requestBody.available();
            }
            byte[] b = new byte[count];
            int readCount = 0;
            while (readCount < count) {
                readCount += requestBody.read(b, readCount, count - readCount);
            }
            String data = new String(b);
            logger.info("[Plugin] EventHandler data:{}", data);

            String response = "";
            httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.getBytes(StandardCharsets.UTF_8).length);
            OutputStream responseBody = httpExchange.getResponseBody();
            OutputStreamWriter writer = new OutputStreamWriter(responseBody, StandardCharsets.UTF_8);
            writer.write(response);
            writer.close();
            responseBody.close();
            logger.info("[Plugin]EventHandler responseBody close");
            this.callback.onResponse(0, data, new byte[0]);
        }
    }

}



