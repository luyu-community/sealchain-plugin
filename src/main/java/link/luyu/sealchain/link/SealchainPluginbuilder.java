package link.luyu.sealchain.link;

import link.luyu.protocol.link.Connection;
import link.luyu.protocol.link.Driver;
import link.luyu.protocol.link.LuyuPlugin;
import link.luyu.protocol.link.PluginBuilder;
import link.luyu.sealchain.service.PluginContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@LuyuPlugin("Sealchain")
public class SealchainPluginbuilder extends PluginBuilder {
    private static Logger logger = LoggerFactory.getLogger(SealchainPluginbuilder.class);

    @Override
    public Connection newConnection(Map<String, Object> properties) {
        try {
            logger.info("[Plugin]newConnection(),properties:{}", properties.toString());
            return new SealchainConnection(properties);
        } catch (Exception e) {
            logger.error("[Plugin]newConnection err:", e);
            return null;
        }
    }

    @Override
    public Driver newDriver(Connection connection, Map<String, Object> properties) {
        try {
            logger.info("[Plugin] newDriver(), properties:{}", properties.toString());
            SealchainDriver driver = new SealchainDriver(connection, properties);
            if(connection instanceof SealchainConnection){
                PluginContext.setInstance(driver);
            }
            return driver;
        } catch (Exception e) {
            logger.error("[Plugin] newDriver err:", e);
            return null;
        }
    }
}